First security services provides private security guards for temporary security, armed/unarmed guards, retail loss prevention, vehicle patrol, special event security, temporary security and more.

Address: 12250 Rockville Pike, Suite 208, Rockville, MD 20852, USA

Phone: 301-468-1110

Website: https://www.firstsecurityservices.com
